import { UserMutation, UserQuery } from 'src/resolvers/User';

export default {
  Query: {
    ...UserQuery,
  },
  Mutation: {
    ...UserMutation,
  },
};
