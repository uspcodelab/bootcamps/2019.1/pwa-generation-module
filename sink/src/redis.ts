import Redis from 'redis';

import { promisifyAll } from 'bluebird';

const redisClient: any = promisifyAll(Redis.createClient({
  host: process.env.REDIS_HOST,
}));

export default redisClient;
