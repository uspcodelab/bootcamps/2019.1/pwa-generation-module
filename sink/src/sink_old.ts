import { Client, Msg } from 'ts-nats';

import natsConnection from 'src/nats';
/*import redis from 'src/redis';*/
/*
async function updateUser(id: string, field: string, payload: string): Promise<void> {
  const userString: string = await redis.getAsync(id);
  const updatedUser: any = JSON.parse(userString);
  updatedUser[field] = payload;
  await redis.setAsync(id, JSON.stringify(updatedUser));
}
*/
async function setupSink(): Promise<any> {
  console.log('🚀 Sink ready');
  const nats: Client = await natsConnection;

  nats.subscribe('new.hackathon', async (_: any, msg: Msg) => {
    console.log('new.hackathon' + JSON.parse(msg.data));
    const userCreated: any = JSON.parse(msg.data);
    const id: string = userCreated.payload;
    /*await redis.setAsync(id, JSON.stringify({ id }));
    */
    console.log(userCreated.payload);
    nats.subscribe(`hackathon.${id}`, () => console.log('new.hackathon'));
    /*async (__: any, userMsg: Msg) => {
      const { type, payload }: any = JSON.parse(userMsg.data);
      const field: string = type
        .replace('updatedUser', '')
        .toLowerCase();

      await updateUser(id, field, payload);
    });*/
  });

  nats.subscribe('new.edition', async (_: any, msg: Msg) => {
    console.log('new.edition' + JSON.parse(msg.data));
    const id: any = JSON.parse(msg.data);
    nats.subscribe(`edition.${id}`, () => console.log('new.edition'));
  });

}

setupSink();
