import { Client, Msg } from 'ts-nats';

import natsConnection from 'src/nats';
import redis from 'src/redis';

async function updateRedis(id: string, field: string, payload: string): Promise<void> {
  const userString: string = await redis.getAsync(id);
  const updatedUser: any = JSON.parse(userString);
  updatedUser[field] = payload;
  await redis.setAsync(id, JSON.stringify(updatedUser));
  console.log(JSON.stringify(updatedUser));
}

async function setupSink(): Promise<any> {
  console.log('🚀 Sink ready');
  const nats: Client = await natsConnection;

  nats.subscribe('new.hackathon', async (_: any, msg: Msg) => {
    const hackathonCreated: any = JSON.parse(msg.data);
    console.log(hackathonCreated);
    const id: string = hackathonCreated.payload;
    await redis.setAsync(id, JSON.stringify({ id }));

    nats.subscribe(`hackathon.${id}`, async (__: any, userMsg: Msg) => {
      const { type, payload }: any = JSON.parse(userMsg.data);
      const field: string = type
        .replace('updated', '')
        .toLowerCase();

      await updateRedis(id, field, payload);
    });
  });
  nats.subscribe('new.edition', async (_: any, msg: Msg) => {
    const editionCreated: any = JSON.parse(msg.data);
    console.log(editionCreated);
    const id: string = editionCreated.payload;
    await redis.setAsync(id, JSON.stringify({ id }));

    nats.subscribe(`edition.${id}`, async (__: any, editionMsg: Msg) => {
      const { type, payload }: any = JSON.parse(editionMsg.data);
      const field: string = type
        .replace('updated', '')
        .toLowerCase();

      await updateRedis(id, field, payload);
    });
  });
}

setupSink();
